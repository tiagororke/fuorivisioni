
let vid_ratio = 1.78;
let video = new Vimeo.Player($('#video_iframe'));

fitVideo();

let permissions_check = video.play();
if (permissions_check !== undefined) {
   permissions_check.then(_ => {
      console.log("permissions are good");
      // Autoplay started!
   }).catch(error => {
      console.log("permissions are bad");
      // setMessage("please enable permissions for audio and video, and refresh");
      // Autoplay was prevented.
      // Show a "Play" button so that user can start playback.
   });
}

if(window.attachEvent) {
    window.attachEvent('onresize', function() {
        fitVideo();
    });
}
else if(window.addEventListener) {
    window.addEventListener('resize', function() {
        fitVideo();
    }, true);
} else {
    //The browser does not support Javascript event binding
}

function fitVideo() {
   let window_w = window.innerWidth;
   let window_h = window.innerHeight;
   let window_r = window_w/window_h;
   let content_r = vid_ratio;
   if(window_r > content_r) {
      // content touches top/bottom
      // stretch to fill width
      let actual_content_width = window_w;
      let actual_content_height = actual_content_width/content_r;
      $("#video_iframe").height(actual_content_height);
   } else {
      // content touches left/right
      // stretch to fill height
      let actual_content_height = window_h;
      let actual_content_width = actual_content_height*content_r;
      $("#video_iframe").width(actual_content_width);
   }
}